set(CRYPTOGRAPHY_MAJOR_SRC 1)
set(CRYPTOGRAPHY_MINOR_SRC 2)
set(CRYPTOGRAPHY_PATCH_SRC 2)

set(CRYPTOGRAPHY_VERSION ${CRYPTOGRAPHY_MAJOR_SRC}.${CRYPTOGRAPHY_MINOR_SRC}.${CRYPTOGRAPHY_PATCH_SRC})
set(CRYPTOGRAPHY_GZ cryptography-${CRYPTOGRAPHY_VERSION}.tar.gz)
set(CRYPTOGRAPHY_SOURCE ${LLNL_URL}/${CRYPTOGRAPHY_GZ})
set(CRYPTOGRAPHY_MD5 a8daf092d0558dac6700d7be93b555e5)

add_cdat_package_dependent(CRYPTOGRAPHY "" "" OFF "CDAT_BUILD_LEAN" OFF)
